# coding: utf-8
# This file is maintained in the nxlog/documentation repository.
require 'asciidoctor'
require 'asciidoctor/extensions'

# The maximum line length must be set with consideration for the PDF output.
# Consider the PDF theme, nesting levels (such as config block inside example
# block inside a list of steps), and space used by the line numbering gutter.
MAX_LINE_LENGTH = 84

def check_nesting_depth(src)
  return if !src.document.attr?('customwarnings')
  if src.parent.context == :admonition or
     not [Asciidoctor::Section, Asciidoctor::Document,
          NilClass].include?(src.parent.parent.parent.parent.class)
    parent = src.parent
    until parent.class.is_a?(Asciidoctor::Document) or not
          parent.title.nil?
        parent = parent.parent
    end
    src.title.nil? ? (title = "") : (title = "titled \"#{src.title}\" ")
    warn "WARNING: [source,#{src.attr('language')}] block #{title}under " +
         "\"#{parent.title}\" is nested too deeply"
  end
end

def check_line_lengths(src)
  return if !src.document.attr?('customwarnings')
  long_line = src.lines.max_by(&:length)
  if long_line.size > MAX_LINE_LENGTH then
    warn "WARNING: line in [source,#{src.attr('language')}] block exceeds " +
         "maximum length of #{MAX_LINE_LENGTH} characters: \"#{long_line}\""
  end
end

class SourceTreeprocessor < Asciidoctor::Extensions::Treeprocessor
  def process(document)
    document.find_by(context: :listing, style: 'source') do |src|
      # This is helpful for testing, for example use [source,log,role="test"]
      #next unless src.attr?('role', 'test')
      if src.attr?('language', 'config') then
        check_nesting_depth(src)
        check_line_lengths(src)
        src.set_attr('linenums', '')
        src.set_attr('language', 'xml')
        if document.backend == 'html5' then
          src.lines.replace(src.apply_subs(src.lines, src.subs))
          src.subs.clear
          src.set_attr('language', 'config')
        end
      elsif src.attr?('language', 'statement') then
        check_nesting_depth(src)
        check_line_lengths(src)
        src.set_attr('linenums', '')
      elsif src.attr?('language', 'log') then
        src.lines.map! {|l| l.gsub(/\t/, '⇥')}
        if document.backend == 'html5' then
          src.lines.replace(src.apply_subs(src.lines, src.subs))
          src.subs.clear
          src.lines.map! {|l| l.gsub(/⇥/, '<span class="tab-marker">⇥</span>')}
          src.lines.map! {|l| l.gsub(/$/, '<span class="line-marker"></span>')}
        elsif document.backend == 'pdf' then
          src.lines.map! {|l| l.gsub(/$/, '↵')}
          src.lines.map! {|l| l.gsub(/⇥/, ' ⇥ ')}
        end
      #elsif ['xml', 'json'].include?(src.attr('language')) then
      end
    end
  end
end

# Example for using this extension:
#
# [manpage]
# --
# include::man_nxlog.adoc[tag=manual_include, leveloffset=+1]
# --
class ManPageBlock < Asciidoctor::Extensions::BlockProcessor
  use_dsl
  named :manpage
  on_context :open
  parse_content_as :compound
  def process(parent, reader, attributes)
    new_lines = []
    while line = reader.read_line do
      if line =~ /^={2,} /
        new_lines << '[discrete]'
      end
      new_lines << line
    end
    wrapper = create_open_block(parent, [], {})
    parse_content(wrapper, new_lines)
  end
end

Asciidoctor::Extensions.register do
  treeprocessor SourceTreeprocessor
  block ManPageBlock
end
